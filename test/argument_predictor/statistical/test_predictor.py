"""
Unit tests for the prediction engine module.
"""

from math import isclose
from typing import List
from kitchensink.utility.splitter import PythonHeuristicSplitter
from argument_predictor.statistical.resolver import Function, PredictionContext
from argument_predictor.statistical.predictor import Predictor
from utility import create_statistics_database


def test__score_names():
    """
    Tests to ensure the `_score_names` static method correctly scores names.
    """

    def assert_close_scores(actual: List[float], expected: List[float], rel_tol=0.01):
        assert len(actual) == len(expected)
        for x, y in zip(actual, expected):
            assert isclose(x, y, rel_tol=rel_tol)

    splitter = PythonHeuristicSplitter()
    terms = [("foo", 0.4), ("bar", 0.35), ("baz", 0.3)]

    # Score the names in descending order when they are an exact match
    # with the terms or no match.
    names = ["foo", "bar", "baz", "name"]
    scores = Predictor._score_names(splitter, names, terms, None)
    assert_close_scores(scores, [0.4, 0.35, 0.3, 0])

    # Score the names in descending order when there is a 50% exact match
    # with the each of the terms or no match.
    names = ["foo_name", "bar_name", "baz_name", "name"]
    scores = Predictor._score_names(splitter, names, terms, None)
    assert_close_scores(scores, [0.40, 0.35, 0.3, 0])

    # Score the names identically when there is a match with the highest
    # probability term.
    names = ["foo_bar", "foo_baz", "foo_name"]
    scores = Predictor._score_names(splitter, names, terms, None)
    assert_close_scores(scores, [0.4, 0.4, 0.4])

    # Score the names based on the matching term with the highest probability.
    names = ["foo_bar", "bar_baz", "bar_name"]
    scores = Predictor._score_names(splitter, names, terms, None)
    assert_close_scores(scores, [0.4, 0.35, 0.35])

    # Score the parameter name properly when there is an exact match.
    names = ["foo", "bar", "baz", "param_name"]
    scores = Predictor._score_names(splitter, names, terms, "param_name")
    assert_close_scores(scores, [0.4, 0.35, 0.3, 1.0])

    # Score the parameter name properly when there is a 50% exact match.
    names = ["foo", "bar", "baz", "name"]
    scores = Predictor._score_names(splitter, names, terms, "param_name")
    assert_close_scores(scores, [0.4, 0.35, 0.3, 0.5])

    # Score the parameter name properly when the names are semantically
    # similar.
    names = ["i", "idx", "j"]
    scores = Predictor._score_names(splitter, names, terms, "i")
    assert_close_scores(scores, [1.0, 0.0, 0.0])

    # Score the names properly when there is not match or similarity.
    names = ["dinosaur", "kenai", "rocky"]
    scores = Predictor._score_names(splitter, names, terms, None)
    assert_close_scores(scores, [0.0, 0.0, 0.0])


def test_predict_argument():
    """
    Tests to ensure the `predict_argument` method returns the most likely
    names in scope and constants from the statistics database.
    """
    # Setup objects common to all tests.
    splitter = PythonHeuristicSplitter()
    function = Function("builtins", "open", params=["file", "mode"])
    context = PredictionContext(function, 1)

    # Ensure predict_argument does not crash with an empty database.
    data = {}
    db = create_statistics_database(context, data)
    predictor = Predictor(db, splitter)
    predictions = predictor.predict_argument(context)
    assert predictions == []

    # Setup more complex database.
    data = {
        1: {
            "constants": [
                ("w", 400 / 1150),
                ("r", 300 / 1150),
                ("wb", 200 / 1150),
                ("rb", 100 / 1150),
            ],
            "terms": [("mode", 100 / 150), ("encoding", 50 / 150)],
        },
    }
    db = create_statistics_database(context, data)
    predictor = Predictor(db, splitter)

    # Ensure only the constants are returned when no names are in scope.
    context.in_scope_names = []
    predictions = predictor.predict_argument(context)
    assert predictions == ["w", "r", "wb", "rb"]

    # Ensure only the constants are returned when no matching names are
    # in scope.
    context.in_scope_names = ["foo", "bar", "baz"]
    predictions = predictor.predict_argument(context)
    assert predictions == ["w", "r", "wb", "rb"]

    # Ensure the matching name is returned when it is in scope.
    # Additionally, ensure the matching name appears in the
    # proper position in the results given its relative
    # score (in this case "w" has a score of 0.34 while encoding
    # has a score of 0.33).
    context.in_scope_names = ["file_encoding", "bar", "baz"]
    predictions = predictor.predict_argument(context)
    assert predictions == ["w", "file_encoding", "r", "wb", "rb"]

    # Ensure the matching parameter name is returned when it is in scope.
    # Additionally, ensure the matching parameter name appears in the
    # proper position in the results given its relative score (in this
    # case "read_mode" has a score of 0.66 while "w" has a score of 0.34).
    context.in_scope_names = ["read_mode", "bar", "baz"]
    predictions = predictor.predict_argument(context)
    assert predictions == ["read_mode", "w", "r", "wb", "rb"]

    # Ensure multiple matching names and constants are returned
    # in the correct order.
    context.in_scope_names = ["read_mode", "file_encoding", "baz"]
    predictions = predictor.predict_argument(context)
    assert predictions == ["read_mode", "w", "file_encoding", "r", "wb", "rb"]

    # Ensure multiple matching names and constants are returned
    # in the correct order.
    context.in_scope_names = ["read_mode", "write_mode", "file_encoding", "baz"]
    predictions = predictor.predict_argument(context)
    assert predictions == [
        "read_mode",
        "write_mode",
        "w",
        "file_encoding",
        "r",
        "wb",
        "rb",
    ]
