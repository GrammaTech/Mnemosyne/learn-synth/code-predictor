"""
Utility functions for unit testing.
"""

from typing import Dict
from argument_predictor.statistical.resolver import PredictionContext
from argument_predictor.statistical.statistics import InMemoryStatisticsDB


def create_statistics_database(
    context: PredictionContext,
    data: Dict,
) -> InMemoryStatisticsDB:
    """
    Create an InMemoryStatistics database for use in testing.

    :param context: prediction context containing the function name and module
    :param data: mapping of [argtype] -> [position|keyword] -> [term|constant] -> count
                 for use in the database
    :return InMemoryStatisticsDB object
    """
    root = dict()
    subroot = root

    subroot[context.function.module] = {}
    subroot = subroot[context.function.module]
    subroot[context.function.name] = {}
    subroot = subroot[context.function.name]

    for argtype, values in data.items():
        subroot[argtype] = subroot.get(argtype, {})
        for position_or_keyword, counts in values.items():
            subroot[argtype][position_or_keyword] = counts

    return InMemoryStatisticsDB(root)
