"""
Unit tests for the statistics database module.
"""

from math import isclose
from typing import Union
from argument_predictor.statistical.resolver import Function, PredictionContext
from argument_predictor.statistical.statistics import InMemoryStatisticsDB
from utility import create_statistics_database

# Helper functions
def get_db_method(argtype: str):
    """
    Return the database method to be used with the given argtype.

    :param argtype: argument type to query for (terms or constants)
    :return InMemoryStatisticsDB function object
    """
    if argtype == "terms":
        return getattr(InMemoryStatisticsDB, "top_arg_terms")
    else:
        return getattr(InMemoryStatisticsDB, "top_arg_constants")


def create_prediction_context(
    position_or_keyword: Union[str, int]
) -> PredictionContext:
    """
    Create a PredictionContext object for use in testing.

    :param position_or_keyword: argument position (integer) or keyword (string)
    :return PredictionContext object
    """
    function = Function("module", "function", params=["argname0"])

    if isinstance(position_or_keyword, int):
        return PredictionContext(function, position=position_or_keyword)
    else:
        return PredictionContext(function, keyword=position_or_keyword)


# Common test implementations
def db_results_retrieval_common(argtype: str, position_or_keyword: Union[str, int]):
    """
    Ensure data can be retrieved from a statistics database for the given argument
    type and position_or_keyword parameter.

    :param argtype: argument type (terms or constants)
    :param position_or_keyword: argument position (integer) or keyword (string)
    """
    # Retrieve the database method to utilize.
    method = get_db_method(argtype)

    # Build the prediction context.
    context = create_prediction_context(position_or_keyword)

    # Create the statistics database.
    expected = [("foo", 1 / 2), ("bar", 1 / 3), ("baz", 1 / 6)]
    data = {
        position_or_keyword: {
            argtype: expected,
        }
    }
    db = create_statistics_database(context, data)

    # Retrieve the scores for the given argument type and prediction context
    # from the database.
    scores = method(db, context)

    # Test the scores versus the expected results.
    for score, truth in zip(scores, expected):
        assert score[0] == truth[0]
        assert isclose(score[1], truth[1])


# Unit tests
def test_db_results_retrieval():
    db_results_retrieval_common("terms", 0)
    db_results_retrieval_common("constants", 0)
    db_results_retrieval_common("terms", "argname0")
    db_results_retrieval_common("constants", "argname0")
