import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="code-predictor",
    author="Grammatech",
    description="A Python module with a collection of techniques to assist a developer during coding with probabilistic code auto-completion.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/code-predictor",
    packages=["argument_predictor.cmd", "argument_predictor.statistical"],
    package_dir={"": "src"},
    python_requires=">=3.6",
    classifiers=[
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    package_data={
        "argument_predictor.statistical": ["resources/callsites.db/*.sqlite3"]
    },
    entry_points={
        "console_scripts": [
            "argument-predictor-server = argument_predictor.cmd.server:main",
        ],
    },
    setup_requires=["wheel"],
    install_requires=[
        "dpcontracts",
        "flask",
        "flask_restful",
        "jedi",
        "pygls",
        "kitchensink @ git+https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink@master",
    ],
    extras_require={"dev": ["black", "flake8", "pre-commit", "pytest"]},
)
