"""
LSP or REST server for probabilistic callarg prediction.
"""

import json
import logging

from typing import List, Text, Tuple

from flask import Flask, Response, request
from flask_restful import Api, Resource
from jedi import Script
from pygls.server import LanguageServer
from pygls.lsp.methods import COMPLETION, TEXT_DOCUMENT_DID_OPEN, TEXT_DOCUMENT_DID_SAVE
from pygls.lsp.types import (
    CompletionItem,
    CompletionList,
    CompletionParams,
    CompletionTriggerKind,
    DidOpenTextDocumentParams,
    DidSaveTextDocumentParams,
)

from kitchensink.utility.parso import (
    ast_at_point,
    ancestor_satisfying_predicate,
    callargs,
    callsite_function,
    callsite_module,
    callsite_signature,
    in_scope_names,
    is_field_access,
    is_incomplete_function_callsite,
    is_incomplete_keyword_argument,
    last_argument_position,
)
from kitchensink.utility.pygls import NonTerminatingLanguageServerProtocol
from kitchensink.utility.server import lsp_arg_parser_with_rest
from kitchensink.utility.splitter import PythonRoninSplitter
from kitchensink.utility.string import subtext
from argument_predictor.statistical.predictor import (
    Predictor,
    DEFAULT_TOP_K,
    DEFAULT_THRESHOLD,
)
from argument_predictor.statistical.resolver import Function, PredictionContext
from argument_predictor.statistical.statistics import StatisticsDB, ZODBStatisticsDB

DEFAULT_PORT = 3036
DEFAULT_STDIO_FLAG = False
DEFAULT_REST_FLAG = False


def predictions(
    script: Script,
    position: Tuple[int, int],
    database: StatisticsDB,
    top_k: int = DEFAULT_TOP_K,
    threshold: float = DEFAULT_THRESHOLD,
) -> List[Text]:
    """
    Return a list of callarg predictions for the incomplete
    function callsite in TEXT at POSITION.

    :param script: parsed text of the program
    :param position: position of the incomplete function callsite in TEXT
                     as a LINE, COL tuple where the LINE is one-indexed
                     and COL is zero-indexed for use with Jedi
    :param database: statistics database of terms/constants collected from
                     a corpus of programs
    :param top_k: number of results to return
    :param threshold: minimum score (between [0.0, 1.0]) for a prediction
    :return list of text callarg predictions
    """
    results = []

    # Get the incomplete function callsite at POSITION.
    root = script._module_node
    node = ast_at_point(root, *position).get_previous_leaf()
    node = ancestor_satisfying_predicate(is_incomplete_function_callsite, node)

    # We are now at an incomplete function callsite.
    if node:
        # Build the callsite function representation.
        signature = callsite_signature(script, node)
        function_name = callsite_function(script, node)
        module = callsite_module(script, node)
        params = [param.name for param in signature.params] if signature else []
        function = Function(module, function_name, params)

        # Build the prediction context
        args = callargs(node)
        if args and is_field_access(args[-1]):
            position = -1
            keyword = None
        elif args and is_incomplete_keyword_argument(args[-1]):
            position = last_argument_position(node)
            keyword = args[-1].value
        else:
            position = last_argument_position(node)
            keyword = None
        names = in_scope_names(node)
        context = PredictionContext(function, position, keyword, names)

        # Perform the prediction.
        predictor = Predictor(database, PythonRoninSplitter())
        results = predictor.predict_argument(context, top_k=top_k, threshold=threshold)

    return results


def main():
    parser = lsp_arg_parser_with_rest(
        desc=__doc__,
        default_port=DEFAULT_PORT,
        default_stdio_flag=DEFAULT_STDIO_FLAG,
        default_rest_flag=DEFAULT_REST_FLAG,
    )

    parser.add_argument(
        "--top-k",
        type=int,
        default=DEFAULT_TOP_K,
        help="Max number of code prediction results to return.",
    )
    parser.add_argument(
        "--threshold",
        type=float,
        default=DEFAULT_THRESHOLD,
        help="Minimum score between 0.0 and 1.0 (inclusive) for a prediction result.",
    )

    args = parser.parse_args()
    database = ZODBStatisticsDB()
    logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

    if args.rest:

        class ArgumentPrediction(Resource):
            def get(self):
                return ""

            def post(self):
                body = request.get_json(force=True)
                text = body.get("text", "")
                lines = text.splitlines()
                position = (len(lines), len(lines[-1]))

                if text:
                    script = Script(text)
                    result = predictions(
                        script,
                        position,
                        database=database,
                        top_k=args.top_k,
                        threshold=args.threshold,
                    )
                else:
                    result = []

                return Response(
                    json.dumps({"result": result}),
                    status=200,
                    mimetype="application/json",
                )

        app = Flask(__name__)
        api = Api(app)
        api.add_resource(ArgumentPrediction, "/argument-prediction")

        app.run("0.0.0.0", port=args.port)
    else:
        if args.stdio:
            server = LanguageServer()
        else:
            server = LanguageServer(protocol_cls=NonTerminatingLanguageServerProtocol)

        # Provide argument predictions when the client requests completions.
        @server.feature(COMPLETION)
        def completions(params: CompletionParams) -> CompletionList:
            workspace = server.workspace
            document = workspace.get_document(params.text_document.uri)
            position = (params.position.line + 1, params.position.character)
            text = subtext(document.source, *position)
            script = Script(text, path=document.path)

            results = []
            trigger = (
                params.context.trigger_kind
                if params.context
                else CompletionTriggerKind.Invoked
            )

            if trigger == CompletionTriggerKind.Invoked:
                results = predictions(
                    script,
                    position,
                    database=database,
                    top_k=args.top_k,
                    threshold=args.threshold,
                )
            results = [CompletionItem(label=prediction) for prediction in results]
            return CompletionList(is_incomplete=False, items=results)

        # Optimization to parse the source code upon open and save.
        # Jedi/parso caches parsing results and only performs parsing
        # on the code differences between parsing requests.  By
        # parsing upon text document open and save, we (hopefully)
        # reduce the number of differences which must be parsed
        # when a completion request is made, speeding the delivery
        # of these completions.
        def cache(server: LanguageServer, uri: str) -> Script:
            workspace = server.workspace
            document = workspace.get_document(uri)
            return Script(document.source, path=document.path)

        @server.feature(TEXT_DOCUMENT_DID_OPEN)
        def did_open(server: LanguageServer, params: DidOpenTextDocumentParams) -> None:
            cache(server, params.text_document.uri)

        @server.feature(TEXT_DOCUMENT_DID_SAVE)
        def did_save(server: LanguageServer, params: DidSaveTextDocumentParams) -> None:
            cache(server, params.text_document.uri)

        # Spawn the server.
        if args.stdio:
            server.start_io()
        else:
            server.start_tcp("0.0.0.0", args.port)


if __name__ == "__main__":
    main()
