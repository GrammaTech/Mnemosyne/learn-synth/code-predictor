from typing import Optional, List, Iterable


class Function:
    """
    represents a defined function.
    """

    def __init__(self, module: str, name: str, params: List[str] = []):
        """
        :param module: the fully qualified module name
        :param name: name of the function
        :param params: the list of parameter names for the function, these are
            expected to be provided in order
        """
        self.module = module
        self.name = name
        self.params = params

    def get_position(self, param_name: str) -> Optional[int]:
        """
        provide the position of the `param_name`, if possible

        :param param_name: name of parameter for this function
        :return: its position in the list of parameters
        """
        if self.params:
            try:
                return self.params.index(param_name)
            except ValueError:
                return None
        return None


class PredictionContext:
    """
    represents the local context for prediction
    """

    def __init__(
        self,
        function: Function,
        position: Optional[int] = None,
        keyword: Optional[str] = None,
        in_scope_names: Iterable[str] = [],
    ):
        """
        :param function: function being invoked
        :param position: position at the callsite of the argument being predicted
        :param keyword: keyword at the callsite of the argument being predicted
        :param in_scope_names: the names in scope - these are one of the sources
            from which we are drawing an argument prediction
        """
        self.function = function
        self.position = position
        self.keyword = keyword
        self.in_scope_names = in_scope_names

    def get_param_name(self) -> Optional[str]:
        """
        get the parameter name corresponding to the context, if available

        :return: optional parameter name
        """
        if self.keyword:
            return self.keyword
        if (
            self.position is not None
            and self.function.params
            and self.position < len(self.function.params)
        ):
            return self.function.params[self.position]
        return None
