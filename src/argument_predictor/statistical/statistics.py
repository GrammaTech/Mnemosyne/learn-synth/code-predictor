from abc import ABC, abstractmethod
from contextlib import contextmanager
from pathlib import Path
from typing import Dict, List, Tuple, Union
from persistent import Persistent
from persistent.mapping import PersistentMapping
from kitchensink.utility.zodb import DatabasePool
from argument_predictor.statistical.resolver import PredictionContext


class StatisticsDB(ABC):
    """
    represents the statistics about name information collected from a corpus
    of programs
    """

    def top_arg_terms(self, context: PredictionContext) -> List[Tuple[str, float]]:
        """
        return at most `limit` number of most frequent argument terms that appear
            in the given `context`

        :param context: the context under which we need to get the top terms
        :return: a list of (term, score) pairs, ordered in descending order of
            score
        """
        with self._yield_data() as data:
            return StatisticsDB._top_common(
                data,
                "terms",
                context,
            )

    def top_arg_constants(self, context: PredictionContext) -> List[Tuple[str, float]]:
        """
        return at most `limit` number of most frequent constants (integer
            and string literals) that appear in the given `context`

        :param context: the context under which we need to get the top constants
        :return: a list of (constant, score) pairs, ordered in descending order of
            score
        """
        with self._yield_data() as data:
            return StatisticsDB._top_common(
                data,
                "constants",
                context,
            )

    @abstractmethod
    @contextmanager
    def _yield_data() -> Union[Dict, PersistentMapping]:
        """
        return the data to be queried in a context manager managing
            all supporting resources
        """
        raise NotImplementedError

    @staticmethod
    def _top_common(
        data: Union[Dict, PersistentMapping],
        argtype: str,
        context: PredictionContext,
    ) -> List[Tuple[str, float]]:
        """
        common implementation of top_arg_terms and top_arg_constants returning
            at most `limit` number of the most frequent items of argtype that
            appear in the given `context`.  The returned value is a list of
            (term|constant, score) pairs, ordered in descending order of score.
            Scores are probabilities; for terms, the probability is based on
            the term appearing versus all other terms while for constants the
            probability is based on the constant appearing versus all other terms
            and constants.  This protects us from returning irrelevant constants
            in locations where terms are far more likely to appear overall.

        :param data: data to query
        :param argtype: argument type to query for (terms or constants)
        :param context: the context under which we need to get the top terms or constants
        :return: a list of (term|constant, score) pairs, ordered in descending order of
            score
        """

        # Retrieve the data for the given module and function.
        data = data.get(context.function.module, {})
        data = data.get(context.function.name, {})

        # Get the argument name (if possible) and position.
        keyword = context.keyword
        param_position = context.function.get_position(keyword)
        position = context.position

        # Attempt to retrieve the results from the database.
        results = data.get(param_position, {}).get(argtype, [])
        if not results:
            results = data.get(keyword, {}).get(argtype, [])
        if not results:
            results = data.get(position, {}).get(argtype, [])

        # Once retrieved, place the data in memory detached from
        # any database connection, if applicable.
        results = results.data if isinstance(results, Persistent) else results

        return results


class InMemoryStatisticsDB(StatisticsDB):
    """
    Statistics database where data in stored in memory.
    """

    def __init__(self, data: Dict = {}):
        self.data = data

    @contextmanager
    def _yield_data(self) -> Dict:
        yield self.data


class ZODBStatisticsDB(StatisticsDB):
    """
    Statistics database where data is stored in a ZODB instance.
    """

    DEFAULT_PATH = Path(__file__).parent / "resources" / "callsites.db"

    def __init__(self, db_path: Path = DEFAULT_PATH):
        if not db_path.exists():
            raise FileNotFoundError(f"ZODB at {db_path} does not exist.")

        self.db = DatabasePool.pool_create_database(db_path)

    @contextmanager
    def _yield_data(self) -> PersistentMapping:
        with self.db.transaction() as conn:
            yield conn.root()
