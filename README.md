# Code Predictor

A Python module with a collection of techniques to assist
a developer during coding with probabilistic code auto-completion.

# Installation

This repository requires python 3.6 or higher.  You will also need
git-lfs which may be installed using the instructions at
https://git-lfs.github.com/.

## Developer Install

If you are working on the development of this repository or
are a client looking for more frequent updates, you may wish
to install the package using the instructions given below.

The dependencies for this repository may be installed by executing
`pip3 install -r requirements.txt`.  If you are contributing you
will want to install the pre-commit hooks found in the
[pre-commit config](./pre-commit-config.yaml) by executing
`pre-commit install`.

After installing dependencies, you must place the repository's
src/ directory on your $PYTHONPATH.  From the base of this
repository, this may be accomplished with the following command:

```
export PYTHONPATH=$PWD/src:$PYTHONPATH
```

Finally, you will need to clone the
[kitchensink](https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink)
repository, install its requirements, and place its src/ directory on
your $PYTHONPATH.  The kitchensink repository is a dependency of this
repository under current development.  This setup may be done with the
following sequence of commands, to be run from the parent directory
of this repository:

```
git clone git@gitlab.com:GrammaTech/Mnemosyne/learn-synth/kitchensink.git
cd kitchensink
pip3 install -r requirements.txt
export PYTHONPATH=$PWD/src:$PYTHONPATH
```

## Client Install

If you are using this library solely as a client, you may
create a python wheel file from this repository and install
it by executing the following sequence of commands:

```
python3 setup.py bdist_wheel --universal
pip3 install dist/*
```

Once installed, an `argument-predictor-server` command will be added
to your $PATH which may be invoked directly using the same interface
and options described in the argument predictor section below.

# Command Line Interface

## Argument Predictor

For developers, the `server.py` script in `src/argument_predictor/cmd`
defines a command line interface for this tool.  Help documentation is
available by running `server.py --help`.

The script operates in two modes - LSP server mode (the default) and REST
mode (when `--rest` is passed).  In LSP mode, the server takes LSP
completion requests at incomplete function callsites and returns
potential completions for the current callarg if any applicable
completions are found.  In REST mode, the server takes POST requests with
a json body like the following: `{"text": "your python code here"}`.  An
example is given below:

```
curl -X POST \
     http://localhost:3036/argument-prediction \
     -H 'Content-Type: application/json' \
     -d '{"text":"import os\nDIRNAME=os.path.dirname(__file__)\nDATA=os.path.join("}'
```

The returned JSON has the form `{"result": List[Completions]}`.

Beyond this, the client may specify the port on which LSP requests are
listened for by specifying the `--port` parameter; alternatively, the
client may pass `--stdio` to listen for requests on stdio in LSP
mode.  Additionally, the client may control the number completions
returned using the `--top-k` parameter and the minimum score for these
completions using the `--threshold` parameter.
